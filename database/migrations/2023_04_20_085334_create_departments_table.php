<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('departments') == false) {
            Schema::create('departments', function (Blueprint $table) {
                $table->id()->autoIncrement();
                $table->string('department_name', 100)->unique();
                $table->string('slug', 100);
                $table->string('date_add', 30);
                $table->string('last_modifer');
                $table->integer('status')->default(1);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
