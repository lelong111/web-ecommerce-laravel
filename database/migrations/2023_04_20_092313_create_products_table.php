<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('products', function (Blueprint $table) {
                $table->id()->autoIncrement();
                $table->string('name', 30)->unique();
                $table->string('image', 100);
                $table->unsignedBigInteger("department_id");
                $table->integer("price");
                $table->integer("discount_flag");
                $table->integer("discounted_price");
                $table->integer('is_top_buy');
                $table->integer('status');
                $table->integer("quantity");
                $table->integer('weight');
                $table->integer('volume');
                $table->longText('description');
                $table->string('date_add');
                $table->string('last_modifer');
//                $table->unsignedBigInteger('department_id');
                $table->foreign('department_id')->references('id')->on('departments');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
