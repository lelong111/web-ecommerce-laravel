<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users') == false) {
            Schema::create('users', function (Blueprint $table) {
                $table->id()->autoIncrement();
                $table->string('first_name', 30);
                $table->string('last_name', 30);
                $table->string('email', 30)->unique();
                $table->string('password', 100);
                $table->string('image', 100);
                $table->string('date_create', 20);
                $table->string('late_modifier', 20);
                $table->integer('role');
                $table->integer('status');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
