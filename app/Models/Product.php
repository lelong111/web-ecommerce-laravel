<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = [
        'name',
        'image',
        'department_id',
        'price',
        'discount_flag',
        'discounted_price',
        'is_top_buy',
        'status',
        'quantity',
        'weight',
        'volume',
        'description',
        'date_add',
        'last_modifer',
    ];
    public $timestamps = false;
}
