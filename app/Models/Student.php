<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $table = "students";
    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
        'phone',
        'created_at',
        'updated_at'
    ];
}
