<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory;

    protected $table = "users";

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'image',
        'date_create',
        'late_modifier',
        'role',
        'status',
        'remember_token'
    ];
    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }
}
