<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index() {
        return view('login');
    }
    public function login(Request $req) {
        $credentials = $req->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
//        if (Auth::attempt(['email' => $req->email, 'password' => $req->password, 'role' => '1'])) {
//            $req->session()->regenerate();
//            $req->session()->flash('flash_message', 'Xin chào '.auth()->user()->first_name. ' '.auth()->user()->last_name);
//            return redirect('/admin/department');
//
//        }
        if (Auth::attempt($credentials)) {
            if (Auth::user()->role == 1) {
                $req->session()->regenerate();
                $req->session()->flash('flash_message', 'Xin chào '.auth()->user()->first_name. ' '.auth()->user()->last_name);
                return redirect('/admin/department');
            }
            $req->session()->regenerate();
            $req->session()->flash('flash_message', 'Xin chào '.auth()->user()->first_name. ' '.auth()->user()->last_name);
            return redirect('/');
//             return redirect()->intended('dashboard');
        }
        $req->session()->flash('error_message', 'Sai tài khoản hoặc mật khẩu');
        return back()->withInput();
    }
    public function register(Request $req) {
        $validator = Validator::make($req->all(),$rules = [
            'inputUser' => 'required|max:30',
            'inputFirstName' => 'required|max:30',
            'inputLastName' => 'required|max:30',
            'inputPassword' => 'required|max:30|confirmed'
        ], $messages = [
            'inputUser.required' => 'Email ko được để trống',
            'inputFirstName.required' => 'Họ không được để trống',
            'inputLastName.required' => 'Tên không được để trống',
            'inputPassword.required' => 'Password không được để trống',
            'inputPassword.confirmed' => 'Mật khẩu không đúng'
        ]);
        if ($validator->fails()) {
            return redirect('/login')->withErrors($validator)
                ->withInput()->with('form', 'register');
        }
        try {
            DB::table("users")->insert([
               'first_name' => $req->inputFirstName,
                'last_name' => $req->inputLastName,
                'password' => bcrypt($req->inputPassword),
                'email' => $req->inputUser,
                'date_create' => time(),
                'role' => 2,
                'status' => 1,
                'image' => '',
                'late_modifier' => '',
            ]);
            $req->session()->flash('flash_message', 'Tạo tài khoản thành công');
        }
        catch (\Illuminate\Database\QueryException $e) {
//            dd($e);
            $req->session()->flash('error_message', 'Tạo tài khoản thất bại');
            return back()->withInput();
        }
        return redirect('login');


    }
    public function logout(Request $request) {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
