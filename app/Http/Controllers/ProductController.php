<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Models\Department;
use MongoDB\Driver\Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    }
    public function index()
    {
        $db = DB::table('products')
            ->join('departments', 'products.department_id', '=', 'departments.id')
            ->select('products.id', 'products.name', 'products.image', 'departments.department_name as department_name', 'products.price', 'products.discount_flag', 'products.discounted_price', 'products.status', 'products.quantity', 'products.weight', 'products.volume', 'products.description', 'products.date_add', 'products.last_modifer')
            ->get()->toArray();
        $db = json_decode(json_encode($db), true);
//        dd($db);
        return view('admin.products',['dataProduct' => $db]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.productAdd', ['dataDepartments' => Department::where('status', 1)->select('department_name', 'id')->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest  $request)
    {
        $request->validated();
        $nameFileCreate = '';
        if ($request->hasFile('image')) {
            if (in_array($request->file('image')->extension(), ['jpg', 'jpeg', 'png']) == false) {
                return back()->withInput();
            }
            $nameFileCreate = basename($request->file('image')->getClientOriginalName(),
                    '.' . $request->file('image')->extension()) . '_' . time() . '.' . $request->file('image')->extension();
            $request->file('image')->storeAs('public/img/product', $nameFileCreate, 'local');
        }
        try {
            $product = new Product();
                $product->name = $request->productName;
                $product->image = $nameFileCreate;
                $product->department_id = $request->departmentID;
                $product->price = $request->price;
                $product->discount_flag = "0";
                $product->discounted_price = $request->discounted_price ?? null;
                $product->is_top_buy = '0';
                $product->status = $request->status;
                $product->quantity = $request->quantity ?? null;
                $product->weight = $request->weight ?? null;
                $product->volume = $request->volume ?? null;
                $product->description = $request->description ?? '';
                $product->date_add = time();
                $product->last_modifer = '';
            $product->save();
        } catch (\Illuminate\Database\QueryException $e)
        {
            $request->session()->flash('error_message', 'Thêm sản phẩm thất bại! Tên sản phẩm đã tồn tại');
            return back()->withInput();
        }
        $request->session()->flash('flash_message', 'Thêm sản phẩm thành công!');
        return redirect('/admin/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        dd(Product::find($id));
        return view('admin.productEdit', ['dataProduct' => Product::find($id), 'departments' => Department::where('status', 1)->get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProductRequest $request, $id)
    {
        $request->validated();
        $nameFileCreate = '';
        if ($request->hasFile('image')) {
            if (in_array($request->file('image')->extension(), ['jpg', 'jpeg', 'png']) == false) {
                return back()->withInput();
            }
            $nameFileCreate = basename($request->file('image')->getClientOriginalName(),
                    '.' . $request->file('image')->extension()) . '_' . time() . '.' . $request->file('image')->extension();
            $request->file('image')->storeAs('public/img/product', $nameFileCreate, 'local');
        }
        try {
            $product = Product::find($id);
//            $product->find($id);
            $product->name = $request->productName;
            if ($nameFileCreate != '') {
                $product->image = $nameFileCreate;
            }
            $product->department_id = $request->departmentID;
            $product->price = $request->price;
            $product->discounted_price = $request->discounted_price ?? null;
            $product->status = $request->status;
            $product->quantity = $request->quantity ?? null;
            $product->weight = $request->weight ?? null;
            $product->volume = $request->volume ?? null;
            $product->description = $request->description ?? '';
            $product->last_modifer = time();
            $product->save();
        } catch (\Illuminate\Database\QueryException $e)
        {
            $request->session()->flash('error_message', 'Sửa sản phẩm thất bại!');
            return back()->withInput();
        }
        $request->session()->flash('flash_message', 'Sửa sản phẩm thành công!');
        return redirect('/admin/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        try {
            Product::find($req->productID)->delete();
            session()->flash('flash_message', 'Xóa thành công!');
        } catch (\Illuminate\Database\QueryException $e){
            session()->flash('error_message', 'Hệ thống đang lỗi!');
        }
        return redirect('admin/ product');
    }
}
