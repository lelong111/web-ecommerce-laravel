<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;


class CartController extends Controller
{
    public function __contruct() {
//        $this->middleware('auth');
    }
    public function addCart(Request $request) {
        if (!$request->session()->has('cart')) {
            $request->session()->put('cart', array());
        }

        $cart = $request->session()->get('cart');
        if (array_key_exists($request->id, $cart)) {
            if ($request->quantity == 'dec') {
                $cart[$request->id] = $cart[$request->id] - 1;
            } else {
                $cart[$request->id] = $cart[$request->id] + 1;
            }
        } else {
            $cart[$request->id] = 1;
        }
        session()->put('cart', $cart);
        print_r(count($request->session()->get('cart')));

    }
    public function check() {
        session()->forget('cart');
//        $a = session('cart');
        echo "<pre>";
//        print_r($a);
    }
    public function removeItemCart(Request $req) {
        session()->forget('cart.'.$req->id);
        $cart = $req->session()->get('cart');
        if ($cart) {
            print_r(count($cart));
        } else {
            echo 0;
        }
    }
    public function cart() {
        $cart = session('cart');
        if ($cart) {
            $listIdProduct = array_keys($cart);
            $listProduct = DB::table('products')
                ->whereIn('id', $listIdProduct)
                ->get();
            $total_price = 0;
            $total_price_pay = 0;
            return view('cart', ['listCart' => $listProduct]);
        }
        return view('cart', ['listCart' => null]);

    }
}
