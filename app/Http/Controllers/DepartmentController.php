<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\Department;
use PHPUnit\Exception;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $dataDepartment = DB::table('department')->all();
//        $dataDepartment = (array) DB::table('departments')->get();
        return view('admin.departments', [ 'dataDepartment' => Department::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.departmentAdd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dateAdd = time();
        $slug = Str::slug($request->departmentName, "-" );
//        $queryStatus;
        try {
            DB::table('departments')->insert([
                'department_name' => $request->departmentName,
                'slug' => $slug,
                'date_add' => $dateAdd,
                'status' => $request->status,
                'last_modifer' => '',
            ]);
            $request->session()->flash('flash_message', 'Thêm danh muc thành công!');

        } catch(\Illuminate\Database\QueryException $e) {
            $request->session()->flash('error_message', 'Thêm danh muc thất bại! Tên danh mục đã tồn tại');
            return back()->withInput();
        }

        return redirect('/admin/department/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.departmentEdit')->with('dataDepartment', Department::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $dateEdit = time();
        $slug = Str::slug($request->departmentName, "-" );
        try {
            DB::table('departments')
                ->where('id', $id)
                ->update([
                    'department_name' => $request->departmentName,
                    'slug' => $slug,
                    'status' => $request->status,
                    'last_modifer' => $dateEdit,
                ]);
            session()->flash('flash_message', 'Sửa thành công!');
        } catch (\Illuminate\Database\QueryException $e) {
            session()->flash('error_message', 'Sửa không thành công');
            return redirect(route('department.edit', $id));
        }
        return redirect('/admin/department/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $a = Department::find($id)->delete();
//        dd($a);
        try {
            Department::find($id)->delete();
            session()->flash('flash_message', 'Xóa thành công!');

        } catch (\Illuminate\Database\QueryException $e) {
            session()->flash('error_message', 'Danh mục này không xóa được');
        }
        return back()->withInput();
    }
}
