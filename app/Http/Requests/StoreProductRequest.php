<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
//    public function authorize()
//    {
//        return false;
//    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'productName' => 'required',
            'departmentID' => 'required',
            'price' => 'required',

        ];
    }
    public function messages()
    {
        return [
            'productName.required' => 'Tên sản phẩm không được để trống',
            'departmentID.required' => 'Bạn chưa chọn danh mục',
            'price.required' => 'Giá tiền không được để trống'
        ];
    }
}
