<?php

// use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
// use App\HTTP\Controllers\StudentController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('student', StudentController::class);
Route::prefix('admin')->group(function () {
    Route::resource('department', DepartmentController::class)->middleware('auth');
    Route::prefix('product')->group(function () {
        Route::get('/', [ProductController::class, 'index'])->name('indexProduct');
        Route::get('create', [ProductController::class, 'create']);
        Route::post('store', [ProductController::class, 'store'])->name('addProduct');
        Route::get('edit/{id}', [ProductController::class, 'edit'] )->name('editProduct');
        Route::post('update/{id}', [ProductController::class, 'update'] )->name('updateProduct');
        Route::post('delete', [ProductController::class, 'destroy'])->name('deleteProduct');
    });
});

Route::get('/login', [LoginController::class, 'index'] )->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::post('/register', [LoginController::class, 'register'])->name('register');
// Route::post('/xuly', [LoginController::class, 'login1']);
Route::get('/', function () {
        $dataProducts = DB::table('products')
            ->join('departments', 'products.department_id', '=', 'departments.id')
            ->select('products.id', 'products.name', 'products.image', 'departments.slug as department_slug', 'products.price', 'products.discount_flag', 'products.discounted_price', 'products.status', 'products.quantity', 'products.weight', 'products.volume', 'products.description', 'products.date_add', 'products.last_modifer')
            ->get()->toArray();
        $dataProducts = json_decode(json_encode($dataProducts), true);
            return view('index', [
                'dataDepartments' => \App\Models\Department::where('status', 1)->get(),
                'dataProducts' => $dataProducts,
            ]);
        }
    );
Route::get('cart',[CartController::class, 'addCart'])->middleware('auth');
Route::get('/go-to-cart', [CartController::class, 'cart'])->middleware('auth');
Route::get('/removeItemCart', [CartController::class, 'removeItemCart'] )->middleware('auth');
Route::get('check', [CartController::class, 'check'] );
Route::get('/logout', [LoginController::class, 'logout']);






Route::get('/test3', function () {
    return view('index1');
});
Route::get('/test', function(){
    return $host = request()->getHost();
});
Route::get('/test2', function() {
    return [1, 2, 3];
});
Route::get('/home', function () {
    return response()
            ->json(['name' => 'Abigail', 'state' => 'CA']);
});
//Route::get('/admin', [AdminController::class, 'index']);
Route::any('/product', function() {});
Route::get('user/{id}');
Route::get('/user', [UserController::class, 'index']);
// Route::resource([
//     'Cate' => CategoryController::class
// ]);
