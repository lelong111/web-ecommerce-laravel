@extends('layoutIndex')
@section('content')
    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Danh mục sản phẩm</span>
                        </div>
                        <ul>
                            @foreach($dataDepartments as $dataDepartment)
                                <li><a href="#">{{ $dataDepartment['department_name'] }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form action="#">
                                <div class="hero__search__categories">
                                    All Categories
                                    <span class="arrow_carrot-down"></span>
                                </div>
                                <input type="text" placeholder="What do yo u need?">
                                <button type="submit" class="site-btn">SEARCH</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>+65 11.188.888</h5>
                                <span>support 24/7 time</span>
                            </div>
                        </div>
                    </div>
                    <div class="hero__item set-bg" data-setbg="img/hero/banner.jpg">
                        <div class="hero__text">
                            <span>FRUIT FRESH</span>
                            <h2>Vegetable <br />100% Organic</h2>
                            <p>Free Pickup and Delivery Available</p>
                            <a href="#" class="primary-btn">SHOP NOW</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Categories Section Begin <section class="categories">--}}
{{--    <div class="container">--}}
    {{--        <div class="row">--}}
    {{--            <div class="categories__slider owl-carousel">--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="categories__item set-bg" data-setbg="img/categories/cat-1.jpg">--}}
    {{--                        <h5><a href="#">Fresh Fruit</a></h5>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="categories__item set-bg" data-setbg="img/categories/cat-2.jpg">--}}
    {{--                        <h5><a href="#">Dried Fruit</a></h5>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="categories__item set-bg" data-setbg="img/categories/cat-3.jpg">--}}
    {{--                        <h5><a href="#">Vegetables</a></h5>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="categories__item set-bg" data-setbg="img/categories/cat-4.jpg">--}}
    {{--                        <h5><a href="#">drink fruits</a></h5>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="categories__item set-bg" data-setbg="img/categories/cat-5.jpg">--}}
    {{--                        <h5><a href="#">drink fruits</a></h5>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--</section>
    {{----}}
        Categories Section End -->
    <!-- Featured Section Begin -->
    <section class="featured spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Featured Product</h2>
                    </div>
                    <div class="featured__controls">
                        <ul>
                            <li class="active" data-filter="*">All</li>
                            @foreach($dataDepartments as $dataDepartment)
                                <li data-filter=".{{ $dataDepartment['slug'] }}">{{ $dataDepartment['department_name'] }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row featured__filter">
                @foreach($dataProducts as $dataProduct)
                    <div class="col-lg-3 col-md-4 col-sm-6 mix {{ $dataProduct['department_slug'] }}">
                        <div class="featured__item">
                            <div class="featured__item__pic set-bg" data-setbg="{{ asset('storage/img/product/'.$dataProduct['image']) }}">
                                <ul class="featured__item__pic__hover">
                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                    <li>
                                        @if (auth()->user())
                                            <a href="javascript:void(0)" onclick="addCart(this.id)" id="{{ $dataProduct['id'] }}"><i class="fa fa-shopping-cart"></i></a>
                                        @else
                                            <a href="javascript:void(0)" onclick="window.location.href='login'"><i class="fa fa-shopping-cart"></i></a>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                            <div class="featured__item__text">
                                <h6><a href="#">{{ $dataProduct['name'] }}</a></h6>
                                <h5>{{ $dataProduct['price'] }}</h5>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
