@extends('layoutIndex')
@section('content')
    <section class="breadcrumb-section set-bg" data-setbg="img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Shopping Cart</h2>
                        <div class="breadcrumb__option">
                            <a href="./index.html">Home</a>
                            <span>Shopping Cart</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                            <tr>
                                <th class="shoping__product">Sản phẩm</th>
                                <th>Giá gốc</th>
                                <th>Giá thanh toán</th>
                                <th>Số lượng</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($listCart)
                                @foreach($listCart as $dataCart)
                                    <tr id="itemCart-{{ $dataCart->id }}">
                                        <td class="shoping__cart__item">
                                            <img src="{{ asset('storage/img/product/'.$dataCart->image) }}" alt="" style="max-height: 100px; max-width: 100px">
                                            <h5>{{ $dataCart->name }}</h5>
                                        </td>
                                        <td class="shoping__cart__price original_price" style="text-decoration: line-through; text-decoration-thickness: 0.2rem; text-decoration-color: red">
                                            {{ $dataCart->price }}
                                        </td>
                                        <td class="shoping__cart__price" id="price-{{ $dataCart->id }}">
                                            {{ $dataCart->discounted_price != "" ? $dataCart->discounted_price : $dataCart->price }}
                                        </td>
                                        <td class="shoping__cart__quantity">
                                            <div class="quantity">
                                                <div class="pro-qty">
                                                    <span class="dec qtybtn" onclick="calculate({{$dataCart->id}}, 'dec')">-</span>
                                                    <input type="text" class="soluong" id="quantity-{{ $dataCart->id }}" value="{{ session('cart.'.$dataCart->id) }}" readonly="true">
                                                    <span class="inc qtybtn" onclick="calculate({{$dataCart->id}}, 'inc')">+</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="shoping__cart__total gia_don_hang" id="priceSub-{{$dataCart->id}}">
                                            {{ $dataCart->discounted_price != "" ? $dataCart->discounted_price *  session('cart.'.$dataCart->id)  : $dataCart->price *  session('cart.'.$dataCart->id)  }}
                                        </td>
                                        <td class="shoping__cart__item__close">
                                            <span class="icon_close" onclick="removeItemCart({{$dataCart->id}})"></span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            @else
                                <tr><h1>Not Found</h1></tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="#" class="primary-btn cart-btn">CONTINUE SHOPPING</a>
                        <a href="#" class="primary-btn cart-btn cart-btn-right"><span class="icon_loading"></span>
                            Upadate Cart</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__continue">
                        <div class="shoping__discount">
                            <h5>Discount Codes</h5>
                            <form action="#">
                                <input type="text" placeholder="Enter your coupon code">
                                <button type="submit" class="site-btn">APPLY COUPON</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="shoping__checkout">
                        <h5>Cart Total</h5>
                        <ul>
                            <li>Tổng tiền <span id="total_price"></span></li>
                            <li>Số tiền phải thanh toán <span id="total_price_pay"></span></li>
                        </ul>
                        <a href="#" class="primary-btn">PROCEED TO CHECKOUT</a>
                    </div>
                </div>
                <script>
                    var original_price = document.getElementsByClassName('original_price')
                    var gia_don_hang = document.getElementsByClassName('gia_don_hang')
                    var soluong = document.getElementsByClassName('soluong')
                    var total_price = 0
                    var total_price_pay = 0
                    for (let i = 0; i < original_price.length; i++) {
                        console.log(original_price[i].innerHTML)
                        console.log(soluong[i].value)
                        total_price += parseInt(original_price[i].innerHTML)*parseInt(soluong[i].value)
                    }
                    for (const data1 of gia_don_hang) {
                        total_price_pay += parseInt(data1.innerHTML)
                    }
                    document.getElementById('total_price').innerHTML = total_price
                    document.getElementById('total_price_pay').innerHTML = total_price_pay
                </script>
            </div>
        </div>
    </section>
@endsection
