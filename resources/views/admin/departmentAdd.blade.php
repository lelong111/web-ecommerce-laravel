@extends('admin.layoutAdmin')
@section('content')
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Thêm danh mục sản phẩm</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Tables</li>
            </ol>
            <div class="card-body col-md-6 mx-auto" style="padding: 0">
                <form action="{{ route('department.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="departmentName">Tên danh mục</label>
                        <input style="width: 75%; display: inline;" type="text" class="form-control" name="departmentName" value="{{ old('departmentName') }}" id="departmentName" aria-describedby="emailHelp" placeholder="Nhập tên danh mục">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Trạng thái</label>
                        <select name="status" style="margin-left: 30px">
                            <option value="0" <?php old('status') == 1 ? 'selected' : '' ?>>Ẩn</option>
                            <option value="1" {{ old('status') == 1 ? 'selected' : ''}}>Hiện</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid px-4">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Your Website 2023</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
@endsection
