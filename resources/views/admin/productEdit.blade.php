@extends('admin.layoutAdmin')
@section('content')
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Sửa sản phẩm</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Tables</li>
            </ol>
            @if ($errors->any())
                <?php
                foreach ($errors->all() as $error)
                    session()->flash('error_message', $error);
                ?>

            @endif
            <div class="card-body col-md-6 mx-auto" style="padding: 0">
                <form action="{{ route('updateProduct', value($dataProduct['id'])) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="productName" class="col-sm-2 col-form-label">Tên danh mục</label>
                        <div class="col-sm-10">
                            <input style="width: 75%; display: inline;" type="text" class="form-control" name="productName" value="{{ $dataProduct['name'] }}" id="productName" aria-describedby="emailHelp" >
                        </div>

                    </div>
                    <div class="form-group row">
                        <label for="department" class="col-sm-2 col-form-label">Danh mục</label>
                        <div class="col-sm-10">
                            <select name="departmentID"  class="custom-select col-sm-6" >
                                @foreach($departments as $valueDepartment)
                                    <option value="{{$valueDepartment['id']}}" {{ $dataProduct['department_id'] == $valueDepartment['id'] ? 'selected' : ''}}>{{ $valueDepartment['department_name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-2 col-form-label">Giá gốc</label>
                        <div class="col-sm-10">
                            <input style="width: 75%; display: inline;" type="text" class="form-control" name="price" value="{{ $dataProduct['price'] }}" id="price"  placeholder="Nhập giá">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="discounted_price" class="col-sm-2 col-form-label">Giá KM</label>
                        <div class="col-sm-10">
                            <input style="width: 75%; display: inline;" type="text" class="form-control" name="discounted_price" value="{{ $dataProduct['discounted_price'] }}" id="discounted_price"  placeholder="Nhập giá KM">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="quantity" class="col-sm-2 col-form-label">Số lượng</label>
                        <div class="col-sm-10">
                            <input style="width: 75%; display: inline;" type="text" class="form-control" name="quantity" value="{{ $dataProduct['quantity'] }}" id="quantity"  placeholder="Nhập số lượng">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="weight" class="col-sm-2 col-form-label">Cân nặng</label>
                        <div class="col-sm-10">
                            <input style="width: 75%; display: inline;" type="text" class="form-control" name="weight" value="{{ $dataProduct['weight'] }}" id="weight"  placeholder="Nhập cân nặng">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="volume" class="col-sm-2 col-form-label">Thể tích</label>
                        <div class="col-sm-10">
                            <input style="width: 75%; display: inline;" type="text" class="form-control" name="volume" value="{{ $dataProduct['volume'] }}" id="volume"  placeholder="Nhập kích thước">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-2 col-form-label">Mô tả</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="description" name="description" rows="5"> {{ $dataProduct['description'] }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="volume" class="col-sm-2 col-form-label">Trạng thái</label>
                        <div class="col-sm-10">
                            <select name="status"  class="custom-select col-sm-6" >
                                <option value="0" {{ $dataProduct['status'] == 0 ? 'selected' : '' }}>Ẩn</option>
                                <option value="1" {{ $dataProduct['status'] == 1 ? 'selected' : '' }}>Hiện</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="image" class="col-sm-2 col-form-label">Hình ảnh</label>
                        <div class="col-sm-10">
                            <input style="width: 75%; display: inline;" type="file" class="form-control" name="image" value="{{ old('image') }}" id="image" >
                            <img src="{{ asset('storage/img/product') }}/{{$dataProduct['image']}}" class="img-thumbnail" style="max-height: 80px; max-weight:80px">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary" style="display: block;margin: 0 auto;">Submit</button>
                </form>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid px-4">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Your Website 2023</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
@endsection
