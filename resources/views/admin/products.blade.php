@extends("admin.layoutAdmin")
@section("content")
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Sản phẩm</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Sản phẩm</li>
            </ol>
            <div class="card-body" style="padding: 0">
                <button type="button" onclick="window.location.href='/admin/product/create'" class="btn btn-primary float-right" style="margin-bottom: 10px">Thêm Sản phẩm</button>
                <table class="table" style="border: 1px solid #E9ECEF" >
                    <thead class="thead-light">
                    <tr>
                        <th scope="col" style="text-align: center">STT</th>
                        <th scope="col" style="text-align: center">Tên Sản phẩm </th>
                        <th scope="col" style="text-align: center">Hình ảnh</th>
                        <th scope="col" style="text-align: center">Danh mục</th>
                        <th scope="col" style="text-align: center">Giá tiền</th>
                        <th scope="col" style="text-align: center">Thông tin</th>
                        <th scope="col" style="text-align: center">Status</th>
                        <th scope="col" style="text-align: center">Thời gian</th>
                        <th scope="col" style="text-align: center">Xử lý</th>
                    </tr>
                    </thead>
                    <?php $i =1; ?>
                    @foreach($dataProduct as $value)
                        <tbody>
                        <tr>
                            <td style="text-align: center">{{ $i++ }}</td>
                            <td style="text-align: center">{{ $value['name'] }}</td>
                            <td style="text-align: center"><img src="{{ asset('storage/img/product/'. $value['image']) }}" alt="..." class="img-thumbnail thumbnailProduct"></td>
                            <td style="text-align: center">{{ $value['department_name']}}</td>
                            <td style="text-align: center">
                                <small class="text-muted"><b>Giá gốc: {{ $value['price'] }}</b></small><br>
                                <small class="text-muted"><b>Giá KM: {{ $value['discounted_price'] ?? 'NULL'}}</b></small>
                            </td>
                            <td style="text-align: center">
                                <small class="text-muted"><b>Khối lượng: {{ $value['weight'] ?? 'NULL'}}</b></small><br>
                                <small class="text-muted"><b>Thể tích: {{ $value['volume'] ?? 'NULL'}}</b></small><br>
                                <small class="text-muted"><b>Số lượng: {{ $value['quantity'] ?? 'NULL'}}</b></small>
                            </td>
                            <td style="text-align: center">{!! $value['status'] == 1 ?  '<span class="badge badge-success">Hiển thị<span>' : '<span class="badge badge-danger">Ẩn </span>' !!}</td>
                            <td style="text-align: center">
                                <small class="text-muted"><b>Ngày nhập: {{ date("h:m d-m-Y", $value['date_add']) }}</b></small><br>
                                <small class="text-muted"><b>Lần sửa cuối: {{ $value['last_modifer'] != '' ? date("h:m d-m-Y", $value['last_modifer']) : "NULL"}}</b></small>
                            </td>
                            <td style="text-align: center">
                                <a class="btn btn-primary" href="{{ route('editProduct', $value['id']) }}">Edit</a>
                                <form action="{{ route('deleteProduct') }}" method="POST" style="display: inline">
                                    @csrf
                                    <input type="hidden" name="productID" value="{{ $value['id'] }}">
                                    <button class="btn btn-danger" onclick="return confirm('Bạn có muốn xóa sản phẩm này')">Delete</button>
                                </form>

                            </td>
                        </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid px-4">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Your Website 2023</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
@endsection
