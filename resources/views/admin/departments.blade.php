@extends("admin.layoutAdmin")
@section("content")
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Tables</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Tables</li>
            </ol>
            <div class="card-body" style="padding: 0">
                <button type="button" onclick="window.location.href='/admin/department/create'" class="btn btn-primary float-right" style="margin-bottom: 10px">Thêm danh mục</button>
                <table class="table" style="border: 1px solid #E9ECEF" >
                    <thead class="thead-light">
                        <tr>
                            <th scope="row" style="text-align: center">STT</th>
                            <th scope="col" style="text-align: center">Danh mục sản phẩm </th>
                            <th scope="col" style="text-align: center">Ngày thêm</th>
                            <th scope="col" style="text-align: center">Ngày sửa gần đây</th>
                            <th scope="col" style="text-align: center">Trạng thái</th>
                            <th scope="col" style="text-align: center">Xử lý</th>
                        </tr>
                    </thead>
                    <?php $i =1; ?>
                    @foreach($dataDepartment as $value)
                    <tbody>
                        <tr>
                            <td style="text-align: center">{{ $i++ }}</td>
                            <td style="text-align: center">{{ $value['department_name'] }}</td>
                            <td style="text-align: center">{{ date("h:m d-m-Y",$value['date_add']) }}</td>
                            <td style="text-align: center">{{  $value['last_modifer'] != '' ? date("h:m d-m-Y", $value['last_modifer']) : '' }}</td>
                            <td style="text-align: center">{{ $value['status'] == 1 ?  'Hiển thị' : 'Ẩn' }}</td>
                            <td style="text-align: center">
                                <a class="btn btn-primary" href="{{ route('department.edit', $value['id']) }}" role="button">Edit</a>
                                <form action="{{ route('department.destroy', $value['id']) }}" method="POST" style="display: inline">
                                @csrf
                                @method('DELETE')
                                    <button class="btn btn-danger" >Delete</button>
                                </form>
{{--                                <a class="btn btn-danger" href="{{ route('department.destroy', $value['id']) }}" role="button">Delete</a>--}}
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid px-4">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Your Website 2023</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
@endsection
