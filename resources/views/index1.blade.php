<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>
<body>
    @extends('layout')

    @section('content')
        <h1 style="text-align:center">Manager Student</h1>
            <div class="col-md-6">
                <a href="{{route('student.create')}}" class="btn btn-primary float-end">Add Student</a>
            </div>
            @if (Session::has('thongbao'))
            <div class="alert alert-success">
                {{Session::get('thongbao')}}
            </div>
            @endif
        <table class="table" align="center" style="width:60vw">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Password</th>
                <th scope="col">Address</th>
                <th scope="col">Phone</th>
                <th scope="col">Time Create</th>
                <th>
                    Action
                </th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($student as $value)
                    <tr>
                        <th scope="row">{{$i++}}</th>
                        <td>{{$value['name']}}</td>
                        <td>{{$value['email']}}</td>
                        <td>{{$value['password']}}</td>
                        <td>{{$value['address']}}</td>
                        <td>{{$value['phone']}}</td>
                        <td>{{$value['create_at']}}</td>
                        <td>
                            <a href="#" style="text-decoration: none; padding: 5px 15px; background-color: #007BFF; color: white; border-radius: 3px">Edit</a>
                            <a href="#" style="text-decoration: none; padding: 5px 6px; background-color: #DC3545; color: white; border-radius: 3px">Delete</a>
                        </td>
                    </tr> 
                @endforeach
                
            </tbody>
        </table>
    @endsection

    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script> --}}
</body>
</html>