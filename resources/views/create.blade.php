@extends('layout')
@section('content')
    <div class="container">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h3>Add Student</h3>
                </div>
                <div class="col-md-6">
                    <a href="{{route('student.index')}}" class="btn btn-primary float-end">Back manager student</a>
                </div>
                <div class="card-body">
                    <form action="{{route('student.store')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="text" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" class="form-control" name="name" id="exampleInputPassword1">
                            </div>
                            <div class="form-group">
                                <label class="exampleInputPassword1">Address</label>
                                <input type="text" class="form-control" name="address" id="exampleInputEmail1" aria-describedby="emailHelp" >
                            
                            </div>
                            <div class="form-group">
                                <label class="exampleInputPassword1">Phone</label>
                                <input type="text" class="form-control" name="phone" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <input type="submit" class="btn btn-primary" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>    
@endsection