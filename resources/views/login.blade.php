<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
    <link href="{{ asset('css/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styleOfMe.css') }}" rel="stylesheet">
    <script src="{{ asset('js/toastr.js') }}"></script>
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Roboto:300);
        .login-page {
          width: 360px;
          padding: 8% 0 0;
          margin: auto;
        }
        .form {
          position: relative;
          z-index: 1;
          background: #FFFFFF;
          max-width: 360px;
          margin: 0 auto 100px;
          padding: 45px;
          text-align: center;
          box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }
        .form input {
          font-family: "Roboto", sans-serif;
          outline: 0;
          background: #f2f2f2;
          width: 100%;
          border: 0;
          margin: 0 0 15px;
          padding: 15px;
          box-sizing: border-box;
          font-size: 14px;
        }
        .form button {
          font-family: "Roboto", sans-serif;
          text-transform: uppercase;
          outline: 0;
          background: #4CAF50;
          width: 100%;
          border: 0;
          padding: 15px;
          color: #FFFFFF;
          font-size: 14px;
          -webkit-transition: all 0.3 ease;
          transition: all 0.3 ease;
          cursor: pointer;
        }
        .form button:hover,.form button:active,.form button:focus {
          background: #43A047;
        }
        .form .message {
          margin: 15px 0 0;
          color: #b3b3b3;
          font-size: 12px;
        }
        .form .message a {
          color: #4CAF50;
          text-decoration: none;
        }
        /*.form .register-form {*/
        /* display: none;*/
        /*}*/
        .container {
          position: relative;
          z-index: 1;
          max-width: 300px;
          margin: 0 auto;
        }
        .container:before, .container:after {
          content: "";
          display: block;
          clear: both;
        }
        .container .info {
          margin: 50px auto;
          text-align: center;
        }
        .container .info h1 {
          margin: 0 0 15px;
          padding: 0;
          font-size: 36px;
          font-weight: 300;
          color: #1a1a1a;
        }
        .container .info span {
          color: #4d4d4d;
          font-size: 12px;
        }
        .container .info span a {
          color: #000000;
          text-decoration: none;
        }
        .container .info span .fa {
          color: #EF3B3A;
        }
        body {
          background: #76b852; /* fallback for old browsers */
          background: rgb(141,194,111);
          background: linear-gradient(90deg, rgba(141,194,111,1) 0%, rgba(118,184,82,1) 50%);
          font-family: "Roboto", sans-serif;
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
        }
    </style>
</head>
<body>
    @if ($errors->all())
        <?php
        foreach ($errors->all() as $message) {
            session()->flash('error_message', $message);
        }
        ?>
    @endif
    @if (Session::has('flash_message'))
        <script>
            toastr.option = {
                "progressBar" : true,
                "closeButton" : true,
            }
            toastr.success("{{ Session::get('flash_message') }}", 'Thành công!', {timeOut:8000});
            // toastr.info
        </script>
    @endif
    @if (Session::has('error_message'))
        <script>
            toastr.option = {
                "progressBar" : true,
                "closeButton" : true,
            }
            toastr.error("{{ Session::get('error_message') }}", 'Thất bại!', {timeOut:8000});
        </script>
    @endif


    <div class="login-page">
        <div class="form">
            <form class="login-form" action="{{ url('login') }}" method="POST" id="login-form">
                @csrf
                <input type="text" placeholder="username" name="email"/>
                <input type="password" placeholder="password" name="password"/>
                <button type="submit">login</button>
                <p class="message">Not registered? <a href="#" onclick="ToggleForm('login')">Create an account</a></p>
            </form>
            <form class="register-form" action="{{ route('register') }}" method="POST" id="register-form" style="display: none">
                @csrf
                <input type="text" placeholder="Nhập email" name="inputUser" value="{{ old('inputUser') }}"/>
                <input type="text" placeholder="Nhập họ" name="inputFirstName" value="{{ old('inputFirstName') }}"/>
                <input type="text" placeholder="Nhập tên" name="inputLastName" value="{{ old('inputLastName') }}"/>
                <input type="password" placeholder="Nhập mật khẩu" name="inputPassword"/>
                <input type="password" placeholder="Nhập lại mật khẩu" name="inputPassword_confirmation"/>
                <button type="submit">Register</button>
                <p class="message">Do you already have an account ? <a href="#" onclick="ToggleForm('register')">Login</a></p>
            </form>
            @if (session()->get('form') == 'register')
                <script>
                    document.getElementById('login-form').style.display = 'none'
                    document.getElementById('register-form').style.removeProperty('display')
                </script>

            @endif

        </div>
      </div>
    <script>
        function ToggleForm(action) {
            if (action == 'login') {
                document.getElementById('login-form').style.display = 'none'
                document.getElementById('register-form').style.removeProperty('display')
            } else {
                document.getElementById('register-form').style.display = 'none'
                document.getElementById('login-form').style.removeProperty('display')
            }
        }
    </script>
</body>
</html>
